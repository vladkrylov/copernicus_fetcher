## Snapshots fetcher from copernicus hub

Provides a quick API to search the snapshots that overlaps the defined region with the smallest time difference.

## Exmaple

See main.py

Don't forget to provide your credentials in config.yaml file in order to auntethicate the search.

### Sample output:

```
Start new search
0: 100 records fetched
1: 100 records fetched
2: 100 records fetched
3: 100 records fetched
4: 75 records fetched

Start new search
0: 100 records fetched
1: 100 records fetched
2: 100 records fetched
3: 100 records fetched
4: 71 records fetched

=== Min time difference ===
1:52:09.745000

{
  "size": "407.88 MB",
  "cloud_coverage": 26.1467,
  "begin_position": "2019-08-09T15:48:19.025000+00:00",
  "instrument": "MSI",
  "end_position": "2019-08-09T15:48:19.025000+00:00",
  "polygon": "MULTIPOLYGON (((-19.58603671492212 78.69885297866472, -19.14390558440905 78.74660611824464, -18.46257511082424 78.81783224260741, -17.99448882389381 78.86504185912936, -17.77885448478519 78.88683129908912, -17.65273227444205 78.89909617854681, -17.07334378403016 78.95552445535996, -16.36632647411321 79.02208765976448, -15.65075301573066 79.08700282497313, -14.92583895820096 79.1500314316895, -14.53387103751188 79.1828065148936, -14.53046528340882 79.2688550122954, -19.80175944852757 79.23228590183592, -19.58603671492212 78.69885297866472)))",
  "quick_view": "https://scihub.copernicus.eu/dhus/odata/v1/Products('07f1b0a9-90a0-40af-8a9d-ae09aa7d1ff7')/Products('Quicklook')/$value",
  "id_": "07f1b0a9-90a0-40af-8a9d-ae09aa7d1ff7"
}
{
  "size": "3.07 GB",
  "cloud_coverage": null,
  "begin_position": "2019-08-09T17:40:28.770000+00:00",
  "instrument": "SAR-C SAR",
  "end_position": "2019-08-09T17:40:42.084000+00:00",
  "polygon": "MULTIPOLYGON (((-20.084255 78.770645, -8.891484 79.591537, -10.185055 80.358749, -22.073503 79.48438299999999, -20.084255 78.770645)))",
  "quick_view": "https://scihub.copernicus.eu/dhus/odata/v1/Products('22aeb241-6f79-4059-8d7e-a47cfcec4bde')/Products('Quicklook')/$value",
  "id_": "22aeb241-6f79-4059-8d7e-a47cfcec4bde"
}
```
