import json
import datetime
from itertools import product

from sentinel import (
    fetcher as f,
    filters,
    polygons,
    models
)


def get_params():
    params = {'start': 0}
    params['footprint'] = f'"Intersects({polygons.greenland_west})"'
    params['beginPosition'] = ('[2018-10-02T00:00:00.000Z TO '
                               '2019-03-09T23:59:59.999Z]')
    return params


def find_nearest():
    user = models.User.load('config.yaml')
    finder = f.SnapshotsFinder(user, limit=1000)

    params = get_params()
    params['cloudcoverpercentage'] = '[0 TO 50]'
    params.update(filters.rgb)
    snapshots_rgb = finder.search(params)
    if not snapshots_rgb:
        print('No RGB data found')
        return

    params = get_params()
    params.update(filters.radar)
    snapshots_radar = finder.search(params)
    if not snapshots_radar:
        print('No radar data found')
        return

    nearest = min(
        product(snapshots_rgb, snapshots_radar),
        key=lambda t: abs(t[0].begin_position - t[1].begin_position)
    )

    print()
    print('=== Min time difference ===')
    print(abs(nearest[0].begin_position - nearest[1].begin_position))
    print()
    print(nearest[0])
    print(nearest[1])


if __name__ == '__main__':
    # find_nearest()

    user = models.User.load('config.yaml')
    finder = f.SnapshotsFinder(user, limit=1000000)
    polygon = polygons.read_geojson('map.geojson')

    params = {
        'start': 0,  # always should be here
        'platformname': 'Sentinel-1',
        'producttype': 'GRD',
        'polarisationmode': 'HV+HH',
        'sensoroperationalmode': 'EW',
        'beginPosition': ('[2018-12-13T00:00:00.000Z TO'
                          ' 2018-12-14T23:59:59.999Z]'),
        'footprint': f'"Intersects({polygon})"'
    }
    snapshots = finder.search(params)

    print(f'\n=== {len(snapshots)} snapshots found ===')
    print('\n'.join([
        f'{s.begin_position}   {s.download_link}'
        for s in sorted(
            snapshots,
            key=lambda dt: dt.begin_position
        )
    ]))
