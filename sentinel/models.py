import os
import json
import datetime
from dataclasses import dataclass

import yaml
import shapely.wkt
import shapely.geometry as g
import marshmallow as mm


@dataclass
class User:
    name: str
    password: str

    @classmethod
    def load(cls, file_name: str):
        if not os.path.isfile(file_name):
            raise FileNotFoundError(f'{file_name} not found!')

        with open(file_name, 'r') as ymlfile:
            cfg = yaml.safe_load(ymlfile)

        if ('ololo' in cfg['auth']['username']
                or 'ololo' in cfg['auth']['password']):
            raise ValueError('Seems like you forgot to put your credentials '
                             f'to {file_name} file.')

        return cls(name=cfg['auth']['username'],
                   password=cfg['auth']['password'])

    @property
    def exists(self):
        return self.name and self.password


class PolygonField(mm.fields.Field):

    def _serialize(self, value: g.Polygon, *args, **kwargs) -> str:
        if value is None:
            return None
        return str(value)

    def _deserialize(self, value: str, *args, **kwargs) -> g.Polygon:
        if not isinstance(value, (str, bytes)):
            raise self.make_error("invalid")
        return shapely.wkt.loads(value)


@dataclass
class Snapshot:
    id_: str
    download_link: str
    begin_position: datetime.datetime
    end_position: datetime.datetime
    quick_view: str
    polygon: g.Polygon
    size: str
    instrument: str
    cloud_coverage: float

    class Schema(mm.Schema):
        id_ = mm.fields.Str()
        download_link = mm.fields.Str()
        begin_position = mm.fields.AwareDateTime()
        end_position = mm.fields.AwareDateTime()
        quick_view = mm.fields.Str()
        polygon = PolygonField()
        size = mm.fields.Str()
        instrument = mm.fields.Str()
        cloud_coverage = mm.fields.Float(allow_none=True)

        @mm.post_load
        def make_object(self, data, **kwargs):
            return Snapshot(**data)

    def __str__(self):
        return json.dumps(self.Schema().dump(self), indent=2)
