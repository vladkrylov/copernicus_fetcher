import time
from typing import List, Dict, Any

import requests

from sentinel.models import User, Snapshot


class SnapshotsFinder:

    def __init__(self, user: User, limit: int = None) -> List[Dict]:
        self.max_iterations = 1000
        self.max_downloaded = limit

        self.user = user
        self.host = (f'https://{self.user.name}:'
                     f'{self.user.password}@scihub.copernicus.eu')

    def search(self, params: Dict[str, Any]) -> List[Snapshot]:
        assert self.user.exists
        print('\nStart new search')

        snapshots = []
        total_items = None
        for i in range(self.max_iterations):
            try:
                r = requests.get(
                    url=f'{self.host}/dhus/search',
                    params=OpenSearchAPI.get_api_params(params),
                )
            except Exception as ex:  # TODO: specify type
                print(type(ex).__name__, ex.args)
                print('No connection, taking a timeout...')
                time.sleep(1)
                continue

            try:
                items = r.json()['feed']['entry']
                if not total_items:
                    total_items = int(
                        r.json()['feed']['opensearch:totalResults']
                    )
            except KeyError:
                break

            for item in items:
                snapshots.append(self.compose_snapshot(item))

            print(f'{i}: {len(items)} records out of {total_items} fetched')
            params['start'] += 100
            if len(snapshots) >= self.max_downloaded:
                break

        return snapshots

    def compose_snapshot(self, response: Dict) -> Snapshot:
        cloud_coverage = None
        if 'double' in response:
            doubles = response['double']
            if isinstance(doubles, dict):
                doubles = [doubles]
            cloud_coverage = self.get_param(doubles, 'cloudcoverpercentage')

        return Snapshot.Schema().load(dict(
            id_=response['id'],
            download_link=response['link'][0]['href'],
            begin_position=self.get_param(response['date'], 'beginposition'),
            end_position=self.get_param(response['date'], 'endposition'),
            quick_view=self.get_param(response['link'], 'icon'),
            polygon=self.get_param(response['str'], 'footprint'),
            size=self.get_param(response['str'], 'size'),
            instrument=self.get_param(response['str'], 'instrumentshortname'),
            cloud_coverage=cloud_coverage
        ))

    def get_param(
            self,
            response: List[Dict[str, Any]],
            param_name: str
    ) -> Any:

        for item in response:
            if item.get('name') == param_name:
                return item.get('content')
            if item.get('rel') == param_name:
                return item.get('href')

        return None


class OpenSearchAPI:
    query_param_names = {
        'platformname',
        'beginposition',
        'endposition',
        'ingestiondate',
        'collection',
        'filename',
        'footprint',
        'orbitnumber',
        'lastorbitnumber',
        'relativeorbitnumber',
        'lastrelativeorbitnumber',
        'orbitdirection',
        'polarisationmode',
        'producttype',
        'sensoroperationalmode',
        'swathidentifier',
        'cloudcoverpercentage',
        'timeliness',
    }

    @staticmethod
    def compose_query_params(params: Dict[str, Any]) -> str:
        return ' AND '.join(f'{k}:{str(v)}' for k, v in params.items())

    def get_api_params(params: Dict[str, Any]) -> Dict[str, Any]:
        query_params = {}
        out_params = {
            'rows': 100,
            'format': 'json'
        }
        for k, v in params.items():
            if k.lower() in OpenSearchAPI.query_param_names:
                query_params[k] = v
            else:
                out_params[k] = v

        out_params['q'] = OpenSearchAPI.compose_query_params(query_params)
        return out_params
