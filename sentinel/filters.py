radar = {
    'platformname': 'Sentinel-1',
    'producttype': 'SLC'
}

rgb = {
    'platformname': 'Sentinel-2',
    'producttype': 'S2MSI2A'
}

sentinel_3 = {
    'platformname': 'Sentinel-3',
}

# rgb = {
#     'platformname': 'Sentinel-3',
#     'instrument': 'OLCI'
# }
